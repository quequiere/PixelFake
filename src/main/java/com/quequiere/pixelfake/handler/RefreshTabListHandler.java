package com.quequiere.pixelfake.handler;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;

import com.quequiere.pixelfake.player.PlayerProfile;

public class RefreshTabListHandler implements Runnable
{

	@Override
	public void run()
	{
	
		for(Player p : Sponge.getServer().getOnlinePlayers())
		{
			PlayerProfile pp = PlayerProfile.getPlayerProfile(p);
			pp.refresh(p);
		}
		
	}

}
