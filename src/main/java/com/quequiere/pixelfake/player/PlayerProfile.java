package com.quequiere.pixelfake.player;

import java.util.ArrayList;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

import com.quequiere.pixelfake.tabs.CustomTab;
import com.quequiere.pixelfake.tabs.OnlineTab;

public class PlayerProfile
{

	private static ArrayList<PlayerProfile> loaded = new ArrayList<PlayerProfile>();


	private String playerName;
	private CustomTab currentTab;

	private PlayerProfile(Player p)
	{
		this.playerName = p.getName();
		this.currentTab=new OnlineTab(p);
	}
	
	

	public CustomTab getCurrentTab()
	{
		return currentTab;
	}



	public void setCurrentTab(CustomTab currentTab)
	{
		this.currentTab = currentTab;
	}



	public static PlayerProfile getPlayerProfile(Player p)
	{
		return getPlayerProfile(p.getName());
	}

	public static PlayerProfile getPlayerProfile(String name)
	{
		for (PlayerProfile pp : loaded)
		{
			if (pp.playerName.equals(name))
			{
				return pp;
			}
		}

		PlayerProfile pp = new PlayerProfile(Sponge.getServer().getPlayer(name).get());
		loaded.add(pp);
		return pp;
	}
	
	
	public void refresh(Player p)
	{
		CustomTab tab = this.getCurrentTab();
		tab.refresh(p);
	}
}
