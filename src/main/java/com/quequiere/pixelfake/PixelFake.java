package com.quequiere.pixelfake;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.scheduler.Task;

import com.quequiere.pixelfake.event.PlayerEvent;
import com.quequiere.pixelfake.handler.RefreshTabListHandler;

@Plugin(id = "pixelfake", name = "PixelFake", version = "1", authors = { "quequiere" }, description = "Pixelfake tab modifier plugin")
public class PixelFake
{

	public static PixelFake plugin;
	private static File file = new File("./config/fakeplayers/username.csv");
	public static ArrayList<String> names = new ArrayList<String>();

	@Listener
	public void onServerStart(GameStartedServerEvent event)
	{
		plugin = this;

		loadFakePlayers();

		Task.Builder taskBuilder = Task.builder();
		taskBuilder.execute(new RefreshTabListHandler()).interval(10, TimeUnit.SECONDS).name("Refresh tab list").submit(plugin);

		Sponge.getEventManager().registerListeners(this, new PlayerEvent());
	}

	public static void loadFakePlayers()
	{
		try
		{
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			try
			{
				String line = br.readLine();

				while (line != null)
				{
					names.add(line.replaceAll("\"", ""));
					line = br.readLine();
				}

				br.close();
				fr.close();
			}
			catch (IOException exception)
			{
				System.out.println("Erreur lors de la lecture : " + exception.getMessage());
			}
		}
		catch (FileNotFoundException exception)
		{
			System.out.println("Le fichier n'a pas ete trouve");
		}
	}

}