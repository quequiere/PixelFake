package com.quequiere.pixelfake.tabs;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.entity.living.player.tab.TabListEntry;
import org.spongepowered.api.profile.GameProfile;
import org.spongepowered.api.profile.GameProfileManager;
import org.spongepowered.api.profile.property.ProfileProperty;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextStyles;

import com.quequiere.pixelfake.bot.PlayerBot;

public class OnlineTab extends CustomTab
{

	public OnlineTab(Player p)
	{
		super(p);
	}

	@Override
	public void refresh(Player p)
	{
		Object[] liste = p.getTabList().getEntries().toArray().clone();
		for (Object ent : liste)
		{
			TabListEntry tle = (TabListEntry) ent;
			if (tle.getProfile().getName().isPresent())
			{

				for (PlayerBot bot : PlayerBot.list)
				{
					if (bot.getName().equals(tle.getProfile().getName().get()))
					{
						p.getTabList().removeEntry(tle.getProfile().getUniqueId());
					}
					else
					{
						// System.out.println(tle.getProfile().getName().get()+"==>"+tle.getProfile().getUniqueId());
					} 

				}
				
			
				if(tle.getProfile().getName().get().equals("quequiere"))
				{
					//System.out.println(tle.getProfile().getPropertyMap().get("textures"));
				}

			}
		}

		GameProfileManager profileManager = Sponge.getServer().getGameProfileManager();

		for (PlayerBot bot : PlayerBot.list)
		{

			
				//GameProfile cata = profileManager.get(bot.getName()).get();
				
				
				GameProfile cata =  profileManager.createProfile(UUID.fromString("ff43643b-8db3-3d10-9e22-0dbf80920dca"), "skin11201");
				
				cata.getPropertyMap().clear(); 
				
		
				String s = "eyJ0aW1lc3RhbXAiOjE0OTM4MTg2NjMyNjQsInByb2ZpbGVJZCI6IjQ1NjZlNjlmYzkwNzQ4ZWU4ZDcxZDdiYTVhYTAwZDIwIiwicHJvZmlsZU5hbWUiOiJUaGlua29mZGVhdGgiLCJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTNlODFiOWUxOWFiMWVmMTdhOTBjMGFhNGUxMDg1ZmMxM2NkNDdjZWQ1YTdhMWE0OTI4MDNiMzU2MWU0YTE1YiJ9LCJDQVBFIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMjJiOWM1ZWE3NjNjODZmYzVjYWVhMzNkODJiMGZhNjVhN2MyMjhmZDMyMWJhNTQ3NjZlYTk1YTNkMGI5NzkzIn19fQ==";
				String signature = "EwgupLq4zDPhq4buDyCk99++OXG5g3awR2XTafqfdpxUEay9evKoth0T9dsc1A5mlSUG6C+rtTuHNTNbkPpBk2iYv9PGse7RG7FlRr0aVGcYB/jQsU3oAh4efu/JCUEvxwoA/6aME0C9MMbmTx49HKBMxI6hl9Rjj/UmwAhcWrtgOeOQBSe9NYPuR9AKlq9TgGkoRxe6MgZ4yrTTErL7dOm0Y+wxRk27InCQWhYaGEb6hzkrz2tDmJDXUNKarGYrrIHak85yqLaDYM+2iDe24Bm5MWFFZLVkX/snF+q0qu2Nq56ULZTpZtahvED/Hdd6FgAP1GRpZ16xCLoBypShNtEOS/ykkdM2WSc0oegYluef4twctdTQS9y6N86b3qmLb4amsh9aGMP9kIuEIXLVk++qRj/z/aViF9SomaCe3G2aLUMnpwiNFk/VKVRRDPLYVOxLYwJ6z8q0J3vQhcvMFqo2fo+HB3qTZyo0dKQ1VgVFho2W9ziEn6Yv6FAiLwQsU09mUPhn1zIDC/bjg3XYk5seALKjT8Zf8BVo/IR2hE7duANd9FHUfzMIetrHwb8BEIx6fU1GOVMV4tuiFigWtJbZA+xxuhq7iuoQWwBemsEdn/ZdXn6zs0Wm7EGFIKhe6xE6r9dDEpJrA5zg7e6d3BYxrOd8cq7bv7t2em7/WFs=";
				ProfileProperty temp = ProfileProperty.of("textures",s,signature);
				cata.getPropertyMap().put("textures",temp );
				
				TabListEntry p1 = TabListEntry.builder().profile(cata).list(p.getTabList()).gameMode(GameModes.SURVIVAL).build();

				p.getTabList().addEntry(p1);
				 
				
		
			


		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date now = new Date();
		String strDate = sdf.format(now);

		double lag = Sponge.getServer().getTicksPerSecond();

		p.getTabList().setHeader(Text.of(TextColors.YELLOW, "<<---  ", TextColors.GRAY, TextStyles.BOLD, "Pixel", TextColors.RED, "Sky  ", TextColors.YELLOW, "--->>\n"));
		p.getTabList().setFooter(Text.of(TextColors.RED, "\nWebsite : ", TextColors.AQUA, "http://Pixelsky-mc.com\n", TextColors.GRAY, "Server time: " + strDate + "\nLag: " + lag + " tps Online: " + p.getTabList().getEntries().toArray().length));

	}

	public static Optional<ProfileProperty> getSkinProperty(String uuid)
	{
		
		String output = readURL("http://sessionserver.mojang.com/" + uuid + "?unsigned=false");

		System.out.println("out:"+output);
		String sigbeg = "[{\"signature\":\"";
		String mid = "\",\"name\":\"textures\",\"value\":\"";
		String valend = "\"}]";

		String value = getStringBetween(output, mid, valend).replace("\\/", "/");
		String signature = getStringBetween(output, sigbeg, mid).replace("\\/", "/");

		return Optional.<ProfileProperty> of(Sponge.getServer().getGameProfileManager().createProfileProperty("textures", value, signature));
	}

	private static String getStringBetween(final String base, final String begin, final String end)
	{

		Pattern patbeg = Pattern.compile(Pattern.quote(begin));
		Pattern patend = Pattern.compile(Pattern.quote(end));

		int resbeg = 0;
		int resend = base.length() - 1;

		Matcher matbeg = patbeg.matcher(base);

		while (matbeg.find())
			resbeg = matbeg.end();

		Matcher matend = patend.matcher(base);

		while (matend.find())
			resend = matend.start();

		return base.substring(resbeg, resend);
	}

	private static String readURL(String url)
	{
		
		try
		{
			
			HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
			
			System.out.println("pass "+url);

			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "SkinsRestorer");
			con.setConnectTimeout(5000);
			con.setReadTimeout(5000);
			con.setUseCaches(false);

			String line;
			StringBuilder output = new StringBuilder();
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

			while ((line = in.readLine()) != null)
				output.append(line);

			in.close();

			return output.toString();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

}
