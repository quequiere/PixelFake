package com.quequiere.pixelfake.tabs;

import org.spongepowered.api.entity.living.player.Player;

public abstract class CustomTab
{
	private String header="Default header";
	private String footer="Default footer";
	private Player player;
	
	public CustomTab(Player p)
	{
		this.player=p;
	}
	
	
	
	public Player getPlayer()
	{
		return player;
	}


	public String getHeader()
	{
		return header;
	}
	public void setHeader(String header)
	{
		this.header = header;
	}
	public String getFooter()
	{
		return footer;
	}
	public void setFooter(String footer)
	{
		this.footer = footer;
	}
	
	public void apply(Player p)
	{
		
	}
	
	public abstract void refresh(Player p);
	
	
}
