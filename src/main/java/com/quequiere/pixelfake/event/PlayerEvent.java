package com.quequiere.pixelfake.event;

import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.network.ClientConnectionEvent;

import com.quequiere.pixelfake.bot.PlayerBot;

public class PlayerEvent
{
	
	@Listener
	public void event(ClientConnectionEvent.Join e)
	{
		PlayerBot.updateBot();
	}

}
