package com.quequiere.pixelfake.bot;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import org.spongepowered.api.Sponge;

import com.quequiere.pixelfake.PixelFake;

public class PlayerBot
{
	public static ArrayList<PlayerBot> list = new ArrayList<PlayerBot>();
	public static int botPercent = 100;

	private String name;

	public PlayerBot(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return "skin11201";
		//return name;
	}

	private static int getOnlineBot()
	{
		return list.size();
	}

	private static int getNeededBot()
	{
		int onlinePlayers = Sponge.getServer().getOnlinePlayers().size();
		double representative = onlinePlayers * (botPercent / 100.0d);
		double reste = representative - getOnlineBot();
		return (int) Math.round(reste);
	}

	private static String getNewName()
	{
		int r = ThreadLocalRandom.current().nextInt(0, PixelFake.names.size() + 1);
		return PixelFake.names.get(r);
	}
	
	public static void addBot()
	{
		String name = getNewName();
		Boolean find = false;
		for(PlayerBot pb:list)
		{
			if(pb.getName().equals(name))
			{
				find = true;
				break;
			}
		}
		
		if(find)
		{
			addBot();
		}
		else
		{
			PlayerBot bot = new PlayerBot(name);
			list.add(bot);
		}
		
	}

	public static void updateBot()
	{
		int needed = getNeededBot();
		boolean remove = false;
		if (needed == 0)
		{
			return;
		}
		else if (needed < 0)
		{
			remove = true;
		}

		for (int x = 0; x < needed; x++)
		{
			if (list.size() > 0 && remove)
			{
				list.remove(0);
			}
			else
			{
				addBot();
			}

		}

	}

}
